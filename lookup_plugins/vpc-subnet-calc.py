# Ansible lookup module to calculate VPC CIDR ranges
#
# Input: network + distribution weights + azs
# Output: dictionary of subnet type with a list of {az:name, cidr: CIDR} dicts
# Strategy is quite simple right now and could use improvement. Right now the number of IPs are divided, rounded into subnets and rescaled to make it fit if necessary.
# There will be slack due to CIDR being annoying, can't be prevented.

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.errors import AnsibleError, AnsibleParserError
from ansible.plugins.lookup import LookupBase
from pprint import pformat
import ipaddress
import math
try:
    from math import gcd
except ImportError as e:
    # Python2
    from fractions import gcd

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()

# Check for log2
def log2(a):
    return (math.log(a) / math.log(2))


if not "log2" in dir(math):
    math.log2 = log2


def getIpSum(outlist):
    ipsum = 0
    for i in outlist:
        if i['prefix'] is None:
            continue
        i['ipsum'] = 2**(32 - i['prefix'])
        ipsum += i['ipsum']
    return ipsum


def pref2ips(prefix):
    return 2**(32 -prefix)


def calculate(network, azs, public, private, db):
    # Count the amount of blocks we need to divide it properly (azcount * minimized_weights_total)
    divcount = (public + private + db) * len(azs)
    resultnets = {
        'public': [],
        'private': [],
        'db': [],
    }
    lastused = 0
    ournet = ipaddress.ip_network(network, strict=False)
    iptotal = ournet.num_addresses
    ipperaz = iptotal / len(azs)
    # Scale weights to IP counts
    divcount = public + private + db
    publicips = public * (ipperaz / divcount)
    privateips = private * (ipperaz / divcount)
    dbips = db * (ipperaz / divcount)
    display.vvv("Ideal IP division: public[{}] private[{}] db[{}] for a total of [{}] per AZ - our net has [{}] ips".format(publicips, privateips, dbips, ipperaz, iptotal))

    # convert IP blocks to matching prefix lengths
    netprefix     = (32.0 - math.log2(iptotal))
    azprefix        = (32.0 - math.log2(ipperaz))
    publicprefix    = (32.0 - math.log2(publicips)) if publicips > 0 else None
    privateprefix = (32.0 - math.log2(privateips)) if privateips > 0 else None
    dbprefix        = (32.0 - math.log2(dbips)) if dbips > 0 else None
    display.vvv("Ideal IP prefixes: public[/{}] private[/{}] db[/{}] which should fit [/{}] per AZ - our net has [/{}] ".format(publicprefix, privateprefix, dbprefix, azprefix, netprefix))

    # Up to here it's easy, but now we need to actually make it fit. First simply round them.
    # Quick hack would be to always round up....
    # Make IP prefix fit, keeping track of minmax
    minmax = 0
    publicprefixr = round(publicprefix,0) if publicips > 0 else None
    minmax += publicprefixr - publicprefix if publicips > 0 else 0
    privateprefixr = round(privateprefix,0) if privateips > 0 else None
    minmax += privateprefixr - privateprefix if privateips > 0 else 0
    dbprefixr = round(dbprefix,0) if dbips > 0 else None
    minmax += dbprefixr - dbprefix if dbips > 0 else 0

    ipsum = 0
    if publicips > 0:
        ipsum += len(azs) * 2**(32 - publicprefixr)
    if privateips > 0:
        ipsum += len(azs) * 2**(32 - privateprefixr)
    if dbips > 0:
        ipsum += len(azs) * 2**(32 - dbprefixr)

    # If minmax > 1 the subnets became smaller than optimal, means IP space is wasted but it should fit.
    # If minmax == 0 the subnets have an ideal fit, unlikely ;)
    # If minmax < 1 the subnets became bigger than optimal, means we'll need to reduce something.
    display.vvv("Rounded IP prefixes: public[/{}] private[/{}] db[/{}] with minmax[{}] for an ipsum of [{}]".format(publicprefixr, privateprefixr, dbprefixr, minmax, ipsum))

    # Fix problems, if minmax < 1 we'll need to shrink stuff down.
    if minmax < 0:
        minmax = 0
        publicprefixr = math.ceil(publicprefix) if publicips > 0 else None
        minmax += publicprefixr - publicprefix if publicips > 0 else 0
        privateprefixr = math.ceil(privateprefix) if privateips > 0 else None
        minmax += privateprefixr - privateprefix if privateips > 0 else 0
        dbprefixr = math.ceil(dbprefix) if dbips > 0 else None
        minmax += dbprefixr - dbprefix if dbips > 0 else 0
        ipsum = 0
        if publicips > 0:
            ipsum += len(azs) * 2**(32 - publicprefixr)
        if privateips > 0:
            ipsum += len(azs) * 2**(32 - privateprefixr)
        if dbips > 0:
            ipsum += len(azs) * 2**(32 - dbprefixr)
        display.vvv("Made IP prefixes fit: public[/{}] private[/{}] db[/{}] with minmax[{}] for an ipsum of [{}]".format(publicprefixr, privateprefixr, dbprefixr, minmax, ipsum))

    # The 'sort2' key determines which subnet type we'll try to increase first when they're both the same size.
    # i.e. when public and db have a /28 that can be upgraded to /27 we can tweak sort2 to make public win.
    outlist = [
        { 'name'  : 'public',
          'prefix': publicprefixr,
          'sort2' : 1,
        },
        { 'name'  : 'private',
          'prefix': privateprefixr,
          'sort2' : 2,
        },
        { 'name'  : 'db',
          'prefix': dbprefixr,
          'sort2' : 0,
        }
    ]

    # Optimize: if we have space to spare maybe we can upscale a block. See how many IPs are left (per AZ) and if it can be added to a block
    # Right now we pick the smallest block first, that way the smaller blocks get upgraded - if we do it the other way around one block will grow a lot and leave the other untouched.
    # Clashes are determined by sort2 key to make a deterministic choice, might be improved later.
    freeips = iptotal - (getIpSum(outlist) * len(azs))
    optimize = True
    while optimize:
        optimize = False
        outlist = sorted(outlist, key=lambda k: (k['prefix'] if k['prefix'] is not None else 0, k['sort2']), reverse=True)
        for i in outlist:
            if i['prefix'] is None:
                continue
            # If amount of ips needed over all AZs for the bigger prefix fits
            if (len(azs) * (pref2ips(i['prefix'] - 1) - pref2ips(i['prefix']))) < freeips:
                i['prefix'] -= 1
                display.vvv("Bumping {} to lower prefix [{}] due to not wanting to waste space!".format(i['name'], i['prefix']))
                freeips = iptotal - (getIpSum(outlist) * len(azs))
                optimize = True # Try another round
                break
            else:
                display.vvv("Can not bump {} to lower prefix [{}] due to not having enough IPs, need {} but have {}...".format(i['name'], i['prefix'] - 1, len(azs) * pref2ips(i['prefix'] - 1), freeips))

    # Sort by prefix length
    #outlist = sorted(outlist, key=lambda k: k['prefix'])
    outlist = sorted(outlist, key=lambda k: (k['prefix'] if k['prefix'] is not None else 33, k['sort2']))

    # Overview
    ipcount = {'total':0}
    for i in outlist:
        if i['prefix'] is None:
            ipcount[i['name']] = 0
            continue
        ipcount[i['name']] = 2**(32 - i['prefix'])
        ipcount['total'] += 2**(32 - i['prefix'])
    display.v("Final IP division: public[{}] private[{}] db[{}] for a total of [{}] per AZ - useable [{}] ips out of [{}] ips -> wasted {} ips".format(ipcount['public'], ipcount['private'], ipcount['db'], ipcount['total'], ipcount['total']*len(azs), iptotal, iptotal - ipcount['total']*len(azs)))

    # Split up the network into real results
    outsubnets = list(ournet.subnets(new_prefix=int(outlist[0]['prefix']))) # Split up in biggest chunks
    #display.vvv("DEBUG BEFORE NETS: {}".format(pformat(outsubnets, indent=4)))
    minprefix = 0
    maxprefix = 32
    for t in outlist:
        for az in azs:
            if t['prefix'] is None:
                continue
            if len(outsubnets) > 0:
                if int(outsubnets[0].prefixlen) != int(t['prefix']):
                    splitnet = outsubnets.pop(0)
                    #display.vvv("ABOUT TO SPLIT NET {} (rest is: {}) for prefix {}".format(splitnet, pformat(outsubnets, indent=4), int(t['prefix'])))
                    outsubnets = list(splitnet.subnets(new_prefix=int(t['prefix']))) + outsubnets
                if outsubnets[0].prefixlen > minprefix:
                    minprefix = outsubnets[0].prefixlen
                if outsubnets[0].prefixlen < maxprefix:
                    maxprefix = outsubnets[0].prefixlen
                resultnets[t['name']].append({'az': az, 'cidr': str(outsubnets.pop(0))})
    display.vv("Result nets: {}".format(pformat(resultnets, indent=4)))
    resultnets['minprefix'] = minprefix
    resultnets['maxprefix'] = maxprefix
    return resultnets


class LookupModule(LookupBase):
    def run(self, terms, variables=None, **kwargs):
        '''
            :arg terms: a dict with lookup to run.
                e.g. {'network': '172.16.0.0/22', 'db': 0, 'private': 2, 'public', 1, azs: ["eu-west-1a", "eu-west-1b"] }
            :kwarg variables: ansible variables active at the time of the lookup
            :returns: Dictionary with requested AZ/CIDR dicts., i.e. {'public':[{'az': 'eu-west-1a', 'cidr':'172.16.0.0/24'}], ...}
        '''

        try:
            terms     = terms[0]
            network = terms['network']
            azs         = sorted(terms['azs'])
            public    = terms['public'] if 'public' in terms else '0'
            private = terms['private'] if 'private' in terms else '0'
            db            = terms['db'] if 'db' in terms else '0'
        except Exception as e:
            raise AnsibleError('\nInvalid input: {}\n\n\'network\' is required and should be a CIDR, i.e. 172.16.0.0/24\n\'azs\' is also required as a list, i.e azs = ["eu-west-1a"]'.format(pformat(terms)))

        # minimize weights (ignore 0s):
        weightdiv = []
        if public > 0:
            weightdiv.append(public)
        if private > 0:
            weightdiv.append(private)
        if db > 0:
            weightdiv.append(db)
        if len(weightdiv) == 0:
            raise AnsibleError('\nIncomplete input: {}\n\n\'network\' is required and should be a CIDR, i.e. 172.16.0.0/24\n\'azs\' is required as a list\nYou must also specify weights for \'public\', \'private\' and \'db\' subnets - they now all default to 0.'.format(pformat(terms)))
        display.v('\nInput: {}'.format(pformat(terms, indent=8)))
 
        result = weightdiv[0]
        for i in weightdiv[1:]:
            result = gcd(result, i)
        display.vvv("Common divider: %s" % (pformat(result)))
        public    = int(public / result)
        private   = int(private / result)
        db        = int(db / result)

        result = calculate(network, azs, public, private, db)
        return [result]
