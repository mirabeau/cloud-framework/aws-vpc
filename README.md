AWS VPC
=======
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create VPC with Cloudformation. AWS resources that will be created are:
* VPCDHCPOptionsAssociation
* VPC
* InternetGateway
* NatGateway (when create_nat_gateway True and public subnets enabled)
  * NatGateway1 -- AZ a
  * NatGateway2 -- AZ b
* NetworkAcl
  * AclPublic
  * AclPrivate  (when private_subnet_weight > 0)
  * AclDatabase (when database_subnet_weight > 0)
* Subnet
  * SubnetPublic1
  * SubnetPublic2
  * SubnetPublicX
  * SubnetPrivate1  (when private_subnet_weight > 0)
  * SubnetPrivate2  (when private_subnet_weight > 0)
  * SubnetPrivateX  (when private_subnet_weight > 0)
  * SubnetDatabase1 (when database_subnet_weight > 0)
  * SubnetDatabase2 (when database_subnet_weight > 0)
  * SubnetDatabaseX (when database_subnet_weight > 0)
* RouteTable
  * RouteTablePublic
  * RouteTablePrivate1  (when private_subnet_weight > 0)
  * RouteTablePrivate2  (when private_subnet_weight > 0)
  * RouteTablePrivateX  (when private_subnet_weight > 0)
  * RouteTableDatabase1 (when database_subnet_weight > 0)
  * RouteTableDatabase2 (when database_subnet_weight > 0)
  * RouteTableDatabaseX (when database_subnet_weight > 0)

![Draw.io](draw.io/services.png)

When create_nat_gateway is set to True and you want public subnets, default routes will be created for the Private and Database RouteTables to
route traffic for 0.0.0.0/0 over the NatGateways.  Two NatGateways will be created for high availability.
All odd route tables will have NatGateway1 configured (i.e. RouteTablePrivate1, RouteTablePrivate3, RouteTableDatabase1, etc)
All even route tables will have NatGateway2 configured.
Note that a public subnet is required for a NAT Gateway to work, so when public is disabled we will not create any NAT Gateway related resources!

When have_aws_gateway is set to True this implies you will deploy our AWS Gateway role or equivalent instead of a NAT Gateway. The ACL rules that allow
private -> public traffic will then still be created for your private subnets to allow it to function.
A case could be made to do this in the AWS Gateway role instead, but then we need to iterate over all subnets there, which we already do here.

The RouteTablePublic will get a default rule to route traffic for 0.0.0.0/0 over the InternetGateway

The ACLs created will get a default allow rule for the Ephemeral port ranges (1024-65535), see https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html#nacl-ephemeral-ports
`In practice, to cover the different types of clients that might initiate traffic to public-facing instances in your VPC, you can open ephemeral ports 1024-65535. However, you can also add rules to the ACL to deny traffic on any malicious ports within that range. Ensure that you place the DENY rules earlier in the table than the ALLOW rules that open the wide range of ephemeral ports.`

Depending on the region two, three, or more AZs will be available and used when automatic subnet calculation is enabled.

Regarding network/subnet layout:
Automatic calculation mode:
You can disable private and database subnets by setting their weights to 0.
If you want a larger database subnet (vs private for example) simply increase the weight by 1 or 2.
All subnets are calculated based on the amount of IPs in the network divided over the AZs and the weighted subnets.
After this they are rounded to make it fit, trying to squeeze as big a subnet in there as possible.
The `lookup_plugins/vpc-subnet-calc.py` script is used to calculate the subnets, feel free to improve it ;)

Alternatively do not supply the weights but set the subnets variable directly. You are required to define public, private and db subnets in that case. (2 of which may be empty).

On another note:
Make sure you really really know what VPC you want regarding subnets before you roll out anything else in it.
Updates to VPC subnets are a disaster - as an example, when you first have public subnets and then decide to drop these this will cause the private subnets to grow in size.
Cloudformation will then try to create the new ones, which fails due to IP space conflicts/overlap and then fail.
Deleting the entire VPC and rolling it out again is of course easy, but not if you have anything inside the VPC already ;)

The VPC tasks are tagged with 'vpc', the cloudformation stack will be created using the "vpc:deploy" tag. You can exclude it to only have it generate the template and preview it before actually deploying it.

Requirements
------------
Ansible version 2.5.4 or higher
Python 2.7.x

Required python modules:
* boto
* boto3
* awscli
* netaddr

Dependencies
------------
* aws-setup

Role Variables
--------------
### _Role specific_
The following defaults are set:
```yaml
cloudformation_tags: {}
tag_prefix         : "mcf"
aws_vpc_params     :
  create_changeset      : "{{ create_changeset }}"

  domain_names          : "{{ internal_route53.domainname }}"

  # can be either /24 or bigger, smaller is not advised and often not possible
  network               : "10.129.92.0/24"

  create_nat_gateway    : True

  public_subnet_weight  : 1
  private_subnet_weight : 2
  database_subnet_weight: 1

  subnets: {}

```
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
environment_type: <environment>
environment_abbr: <environment abbriviation>
internal_route53:
  domainname: <name of internal domain>
```
Example Playbooks
----------------
Create a cloudformation stack for a test VPC in the 'my-dta' aws account.
The VPC will be created with subnet 10.129.92.0/24
There will be private subnets and public subnets, but not database subnets and no NAT gateway either.
The resulting subnet sizes will be automatically calculated and result in /26 private subnets and /28 public subnets (we have 3 AZs in eu-west-1).
```yaml
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    aws_region      : "eu-west-1"
    owner           : "a-company"
    account_name    : "a-com-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
    aws_vpc_params:
      network               : "10.129.92.0/24"
      create_nat_gateway    : False
      public_subnet_weight  : 1
      private_subnet_weight : 3
      database_subnet_weight: 0

  roles:
    - aws-vpc
```
It is also possible to create a VPC with manually defined subnets. In this case you must define at least 2 AZs per subnet type if you want that subnet type.
This example shows a VPC configuration with public and database subnets, but no private subnet. Note that public only uses 2 out of 3 possible AZs.
```yaml
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    tag_prefix      : "mcf"
    aws_region      : "eu-west-1"
    owner           : "a-company"
    account_name    : "a-com-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
    aws_vpc_params:
      network               : "10.10.176.0/24"
      create_nat_gateway    : False
      subnets:
        db:
          - az: "eu-west-1a"
            cidr: "10.10.176.192/29"
          - az: "eu-west-1b"
            cidr: "10.10.176.200/29"
          - az: "eu-west-1c"
            cidr: "10.10.176.208/29"
        public:
          - az: "eu-west-1a"
            cidr: "10.10.176.16/29"
          - az: "eu-west-1b"
            cidr: "10.10.176.0/29"
        private: {}

  roles:
    - aws-vpc
```

License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
